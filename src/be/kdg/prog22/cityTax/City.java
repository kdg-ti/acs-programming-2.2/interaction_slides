package be.kdg.prog22.cityTax;

import java.util.ArrayList;
import java.util.List;

public class City {
	private static double TAX_SURCHARGE=0.07;
	private List<Person> citizens= new ArrayList<>();
	private String name;

	public City(String name) {
		this.name = name;
	}

	public double getCityTaxes(){
		double total=0.0;
		for (int i = 0; i < citizens.size(); i++) {
			total+=citizens.get(i).getTax();
		}
		return total*TAX_SURCHARGE;
	}

	public double getCityTaxesForEach(){
		double total=0.0;
		for (Person p : citizens) {
			total+=p.getTax();
		}
		return total*TAX_SURCHARGE;
	}

	public void addCitizen(Person person){
		citizens.add(person);
	}
}
