package be.kdg.prog22.cityTax;

public class Person {
	private TaxDeclaration taxDeclaration;
	private String name;

	public Person(String name){
		this.name=name;
	}
	public double getTax() {
		return taxDeclaration.getTax();
	}

	public TaxDeclaration getTaxDeclaration() {
		return taxDeclaration;
	}

	public void setTaxDeclaration(TaxDeclaration taxDeclaration) {
		this.taxDeclaration = taxDeclaration;
	}

	public String getName() {
		return name;
	}


}
