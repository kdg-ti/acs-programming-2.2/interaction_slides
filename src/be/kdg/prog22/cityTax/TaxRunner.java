package be.kdg.prog22.cityTax;

import be.kdg.prog22.cityTax.*;

public class TaxRunner {
	public static void main(String[] args) {
		City antwerp = loadData();
		System.out.println(String.format("fori taxes: %.2f",antwerp.getCityTaxes()));
		System.out.println(String.format("fore taxes: %.2f",antwerp.getCityTaxesForEach()));
	}

	private static City loadData() {
		City city = new City("Antwerp");
		city.addCitizen(loadPerson("George", 20_000.10));
		city.addCitizen(loadPerson("Michael", 10_000.20));
		return city;
	}

	private static Person loadPerson(String name, double tax) {
		Person person = new Person(name);
		TaxDeclaration georgeTax = new TaxDeclaration();
		georgeTax.setTax(tax);
		person.setTaxDeclaration(georgeTax);
		return person;
	}
}
