package be.kdg.prog22.loopBreak;

public class Break {
	public static void main(String[] args) {
		Rekening zicht = new Rekening(-500);
		double debt = zicht.getBalance();
		for (int i = 0; i < 11; ) {
			System.out.println(String.format("Debt (%2d): %7.2f",++i , debt));
      // earning some money, lowering debt
			debt += Math.random() * 100  ;
			if (debt > 0) {
				zicht.save(debt);
				System.out.println(String.format("Positive balance of €%.2f" , debt));
				break;
			}
		}
		if (debt<0){
			System.out.println(String.format("Not enough money to clear balance" ));
		}
	}
}
// sample run
// Debt ( 1): -500,00
//Debt ( 2): -406,72
//Debt ( 3): -405,66
//Debt ( 4): -387,70
//Debt ( 5): -306,87
//Debt ( 6): -270,88
//Debt ( 7): -227,36
//Debt ( 8): -135,82
//Debt ( 9):  -62,32
//Debt (10):  -13,33
//Positive balance of €16,33
