package be.kdg.prog22.loopBreak;

public class Rekening {
	public double getBalance() {
		return balance;
	}

	public void save(double balance) {
		this.balance = balance;
	}

	private double balance;

	public Rekening(double balance) {
		this.balance = balance;
	}
}
